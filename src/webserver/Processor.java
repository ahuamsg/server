package webserver;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;

public class Processor extends Thread {
	private Socket socket;
	private InputStream in;
	private PrintStream out;
	private static String WEB_ROOT = "D:\\file\\"; 

	public Processor(Socket scoket) {
		this.socket = scoket;
		try {
			/* 获取输入流 */
			in = socket.getInputStream();
			out = new PrintStream(socket.getOutputStream());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	/* 复写线程的run方法 */
	public void run() {
		String fileName = parse(in);
		sendFile(fileName);
	}

	/* 解析接受的信息 */
	public String parse(InputStream in) {
		/* 字符流转成字节流 */
		BufferedReader br = new BufferedReader(new InputStreamReader(in));
		String fileName = null;
		try {
			/* 获取浏览器的请求信息 */
			String httpMessage = br.readLine();
			String[] content = httpMessage.split(" ");
			if(content.length != 3){
				sendErrorMessage(400,"Client query error!");
				return null;
			}
			System.out.println("code:" + content[0] + ",filename:" + content[1]
					+ ",http version:" + content[2]);
			fileName = content[1];
		} catch (IOException e) {
			e.printStackTrace();
		}
		return fileName;
	}

	/* 错误处理 */
	public void sendErrorMessage(int errorCode, String errorMsg) {
		out.println("HTTP/1.0 "+errorCode+" "+errorMsg);
		out.println("content-type: text/html");
		out.println();
		out.println("<html>");
		out.println("<title>Error</title>");
		out.println("<body>");
		out.println("<h1> ErrorCode:"+errorCode+",Message:"+errorMsg+"</h1>");
		out.println("</body>");
		out.println("</html>");
		out.flush();
		out.close();
		try {
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void sendFile(String fileName) {
		/* 服务气端，找到请求的文件*/
		File file = new File(this.WEB_ROOT+fileName);
		if(!file.exists()){
			sendErrorMessage(404,"File Not Found");
			return ;
		}
		try {
			/* 将文件都进来*/
			InputStream in = new FileInputStream(file);
			byte[] content = new byte[(int)file.length()];
			in.read(content);
			out.println("HTTP/1.0 200 send queryfile");
			out.println("content-length:"+content.length);
			out.println();
			out.write(content);
			out.flush();
			out.close();
			in.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
