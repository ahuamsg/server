package webserver;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class WebServer {

	public WebServer() {

	}

	public void serverStart(int port) {
		try {
			// 创建端口监听
			ServerSocket serverSocket = new ServerSocket(port);
			System.out.println("系统启动中...");
			while (true) {
				System.out.println("监听端口8080中...");
				Socket scoket = serverSocket.accept();
				new Processor(scoket).start();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		int port = 8080;
		if (args.length == 1) {
			port = Integer.parseInt(args[0]);
		}
		new WebServer().serverStart(port);
	}

}
